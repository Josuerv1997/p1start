package edu.uprm.cse.datastructures.cardealer;



import java.util.Optional;
import java.util.function.Predicate;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarManager {

	private final CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance();

	@GET

	//@Produces(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] cars= new Car[cList.size()];
		for (int i = 0; i < cars.length; i++) {
			cars[i]=cList.get(i);
		}
		return cars;
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		for (int i = 0; i < cList.size(); i++) {
			if(cList.get(i).getCarId()==id) {
				return cList.get(i);
			}
		}
		throw new WebApplicationException(404);
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car Car){
		cList.add(Car);
		return Response.status(201).build();
	}

	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car Car){
		for (int i = 0; i < cList.size(); i++) {
			if(cList.get(i).getCarId()==Car.getCarId()) {
				cList.remove(i);
				cList.add(Car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){
		for (int i = 0; i < cList.size(); i++) {
			if(cList.get(i).getCarId()==id) {
				cList.remove(i);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}