package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
	int size;
	Comparator<E> comp;
	private cirNode<E> header;
	private class cirNode<E>{
		cirNode<E> next;
		cirNode<E> prev;
		E element;
		public cirNode(){
			this.next=null;
			this.prev=null;
			this.element=null;
		}
		public cirNode(cirNode<E> next,cirNode<E> prev,E element) {
			this.next=next;
			this.prev=prev;
			this.element=element;
		}
		public cirNode<E> getNext() {
			return next;
		}
		public void setNext(cirNode<E> next) {
			this.next = next;
		}
		public cirNode<E> getPrev() {
			return prev;
		}
		public void setPrev(cirNode<E> prev) {
			this.prev = prev;
		}
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
	}
	public CircularSortedDoublyLinkedList(Comparator<E> compa){
		header=new cirNode<E>();
		header.setNext(header);
		header.setPrev(header);
		size=0;
		this.comp=compa;
	}
	private class Next implements Iterator<E>{
		private cirNode<E> curr=header.getNext();
		@Override
		public boolean hasNext() {
			return curr!=header;
		}

		@Override
		public E next() {
			E cur=curr.getElement();
			curr=curr.getNext();
			return cur;
		}

	}
	public Iterator<E> iterator() {
		return new Next();
	}

	@Override
	public boolean add(E obj) {
		if(obj==null) {
			return false;
		}
		cirNode<E> temp=header.getNext();
		cirNode<E> n=new cirNode<E>(null,null,obj);
		for(int i=0;i<this.size;i++) {
			if(this.comp.compare(obj, temp.getElement())<0) {
				n.setPrev(temp.getPrev());
				temp.getPrev().setNext(n);
				temp.setPrev(n);
				n.setNext(temp);
				break;
			}
			temp=temp.getNext();
		}
		if(temp==header) {
			n.setPrev(temp.getPrev());
			temp.getPrev().setNext(n);
			temp.setPrev(n);
			n.setNext(temp);
		}
		size++;
		return true;
	}

	@Override
	public int size() {
//		int r=0;
//		cirNode<E> temp=header.getNext();
//		while(!temp.equals(header)) {
//			r++;
//			temp=temp.getNext();
//		}
		return this.size;
	}

	@Override
	public boolean remove(E obj) {
		cirNode<E> temp=header.getNext();
		while(temp!=header&&!temp.getElement().equals(obj)) {
			temp=temp.getNext();
		}
		if(temp!=header) {
			temp.getNext().setPrev(temp.getPrev());
			temp.getPrev().setNext(temp.getNext());
			temp.setNext(null);
			temp.setPrev(null);
			size--;
			return true;
		}
		return false;
	}
	// returns false if index does not exist
	@Override
	public boolean remove(int index) {
		if(index<0||this.size<=index) {
			throw new IndexOutOfBoundsException("index has to be between -1 and the size of the list");
		}
		if(this.size-1<index) {
			return false;
		}
		cirNode<E> temp=header.getNext();
		for(int i=0;i<index;i++) {
			temp=temp.getNext();
		}
		temp.getNext().setPrev(temp.getPrev());
		temp.getPrev().setNext(temp.getNext());
		temp.setNext(null);
		temp.setPrev(null);
		size--;
		return true;
	}

	@Override
	public int removeAll(E obj) {
		int r=0;
		while(this.remove(obj)) r++;
		return r;
	}

	@Override
	public E first() {
		return header.getNext().getElement();
	}

	@Override
	public E last() {
		return header.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		cirNode<E> temp=header.getNext();
		for (int i = 0; i < index; i++) {
			temp=temp.getNext();
		}
		return temp.getElement();
	}
	//Clears the list
	@Override
	public void clear() {
		while(this.size!=0) {
			this.remove(0);
		}
	}
	//Asks if e is contained in the list
	@Override
	public boolean contains(E e) {
		cirNode<E> temp=header.getNext();
		while(temp!=header&&!temp.getElement().equals(e)) {
			temp=temp.getNext();
		}
		return temp.getElement()!=null;
	}

	@Override
	public boolean isEmpty() {
		return this.size==0;
	}
	//looks for the last index of e in the list; returns -1 if not found
	@Override
	public int firstIndex(E e) {
		int r=0;
		cirNode<E> temp=header.getNext();
		while(temp!=header&&!temp.getElement().equals(e)) {
			temp=temp.getNext();
			r++;
		}
		if(this.size==r) {
			return -1;
		}
		return r;
	}
	//looks for the last index of e in the list; returns -1 if not found
	@Override
	public int lastIndex(E e) {
		int r=this.size-1;
		cirNode<E> temp=header.getPrev();
		while(temp!=header&&!temp.getElement().equals(e)) {
			temp=temp.getPrev();
			r--;
		}
		return r;
	}
}
