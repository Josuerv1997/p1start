package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{

	@Override
	public int compare(Car arg0, Car arg1) {
		if(arg1.equals(null)) {
			return -1;
		}
		if(arg0.getCarBrand().compareTo(arg1.getCarBrand())<0) {
			return -1;
		}
		if(arg0.getCarBrand().compareTo(arg1.getCarBrand())>0) {
			return 1;
		}
		if(arg0.getCarModel().compareTo(arg1.getCarModel())<0) {
			return -1;
		}
		if(arg0.getCarModel().compareTo(arg1.getCarModel())>0) {
			return 1;
		}
		if(arg0.getCarModelOption().compareTo(arg1.getCarModelOption())<0) {
			return -1;
		}
		if(arg0.getCarModelOption().compareTo(arg1.getCarModelOption())>0) {
			return 1;
		}
		return 0;
	}
	
}
